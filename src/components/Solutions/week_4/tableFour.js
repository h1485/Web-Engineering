import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";
import TaskThree from "./TaskThree";

export default [
  new Component("FUNCTIONS", TaskOne),
  new Component("OBJECTS", TaskTwo),
  new Component("FIBONACCI", TaskThree),
];
