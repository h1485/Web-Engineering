import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";
import TaskThree from "./TaskThree";

export default [
  new Component("STATIC", TaskOne),
  new Component("MISTAKES", TaskTwo),
  new Component("DENO", TaskThree),
];
