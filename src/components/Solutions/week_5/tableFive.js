import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";

export default [
  new Component("PERFORMANCE", TaskOne),
  new Component("SPEAKERSLIST", TaskTwo),
];
