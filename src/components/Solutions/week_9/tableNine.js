import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";

export default [
  new Component("HANDMADE", TaskOne),
  new Component("INTERACTIVE", TaskTwo),
];
