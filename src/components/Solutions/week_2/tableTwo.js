import TaskOne from "./TaskOne.vue";
import TaskTwo from "./TaskTwo.vue";
import TaskThree from "./TaskThree.vue";
import { Component } from "../../../types";

export default [
  new Component("SELECTORS", TaskOne),
  new Component("POSITIONING", TaskTwo),
  new Component("WIREFRAME", TaskThree),
];
