import TaskOne from "./TaskOne.vue";
import TaskTwo from "./TaskTwo.vue";
import TaskThree from "./TaskThree.vue";
import TaskFour from "./TaskFour.vue";
import { Component } from "../../../types";

export default [
  new Component("WWW", TaskOne),
  new Component("HTTP", TaskTwo),
  new Component("HTML", TaskThree),
  new Component("WIREFRAME", TaskFour),
];
