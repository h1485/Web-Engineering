import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";

export default [
  new Component("STATIC", TaskOne),
  new Component("MISTAKES", TaskTwo),
];
