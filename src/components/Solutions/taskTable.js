import tableOne from "./week_1/tableOne";
import tableTwo from "./week_2/tableTwo";
import tableThree from "./week_3/tableThree";
import tableFour from "./week_4/tableFour";
import tableFive from "./week_5/tableFive";
import tableSix from "./week_6/tableSix";
import tableSeven from "./week_7/tableSeven";
import tableEight from "./week_8/tableEight";
import tableNine from "./week_9/tableNine";
import tableTen from "./week_10/tableTen";
import tableEleven from "./week_11/tableEleven";

export default [
  tableOne,
  tableTwo,
  tableThree,
  tableFour,
  tableFive,
  tableSix,
  tableSeven,
  tableEight,
  tableNine,
  tableTen,
  tableEleven,
];
