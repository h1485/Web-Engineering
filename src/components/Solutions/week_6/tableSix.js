import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";
import TaskThree from "./TaskThree";
import TaskFour from "./TaskFour";
import TaskFive from "./TaskFive";

export default [
  new Component("PARENTHESIS", TaskOne),
  new Component("ITERATOR", TaskTwo),
  new Component("GENERATOR", TaskThree),
  new Component("PROXY", TaskFour),
  new Component("DEEPCOPY", TaskFive),
];
