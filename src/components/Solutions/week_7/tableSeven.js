import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";

export default [
  new Component("FUNCTIONS", TaskOne),
  new Component("FILTER", TaskTwo),
];
