import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";
import TaskThree from "./TaskThree";

export default [
  new Component("FLEXBOX", TaskOne),
  new Component("GRID", TaskTwo),
  new Component("LANDING PAGE", TaskThree),
];
