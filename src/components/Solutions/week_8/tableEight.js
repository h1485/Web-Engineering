import { Component } from "../../../types";
import TaskOne from "./TaskOne";
import TaskTwo from "./TaskTwo";

export default [
  new Component("FETCH / THEN", TaskOne),
  new Component("ASYNC / AWAIT", TaskTwo),
];
