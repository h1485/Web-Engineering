import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Solutions from "../views/Solutions.vue";
import About from "../views/About.vue";
import Links from "../views/Links.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/solution/:id",
    name: "Solutions",
    component: Solutions,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/links",
    name: "Links",
    component: Links,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
