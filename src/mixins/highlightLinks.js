export default {
  methods: {
    highlightClickedItem(e, selector = "") {
      // remove other active items
      const sidebarLinks = document.querySelectorAll(selector);
      sidebarLinks.forEach((item) => {
        item.setAttribute("data-clicked", "0");
      });
      // make the target active
      e.target.setAttribute("data-clicked", "1");
    },

    setActiveSection(i = 0) {
      return window.location.pathname.split("/").slice(-1) == i ? "1" : "0";
    },

    setActiveSubsection(hash = "#1") {
      if (window.location.hash) {
        return window.location.hash === hash ? "1" : "0";
      } else {
        return hash === "#1" ? "1" : "0";
      }
    },
  },
};
